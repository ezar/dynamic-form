import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { InputType } from 'src/app/enums/input-type';
import { Results } from 'src/app/models/Results';
import { Question } from 'src/app/models/question';

@Component({
  selector: 'app-flexible-form',
  templateUrl: './flexible-form.component.html',
  styleUrls: ['./flexible-form.component.css']
})
export class FlexibleFormComponent implements OnInit {

  constructor() {
    this.control = this.questions[this.questionIndex]
  }

  control: Question
  inputType = InputType;
  questionIndex: number = 0
  isLoading: boolean = false

  results: Results[] = []

  questions: Question[] = [
    {
      label: 'Email',
      field: new FormControl('', [Validators.required, Validators.email]),
      errors: {
        required: 'Email wajib diisi',
        email: 'Format email tidak valid'
      },
      inputType: InputType.text
    },
    {
      label: 'Nama lengkap',
      field: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(50)]),
      errors: {
        required: 'Nama lengkap wajib diisi',
        minlength: 'Nama lengkap minimal 1 karakter',
        maxlength: 'Nama lengkap maksimal 50 karakter',
      },
      inputType: InputType.text
    },
    {
      label: 'Nomor KTP',
      field: new FormControl('', [Validators.required, Validators.pattern("^[0-9]*$"), Validators.minLength(1), Validators.maxLength(200),]),
      errors: {
        required: 'Nomor KTP wajib diisi',
        minlength: 'Nomor KTP minimal 1 karakter',
        maxlength: 'Nomor KTP maksimal 200 karakter',
        pattern: 'Nomor KTP harus diisi dengan angka',
      },
      inputType: InputType.text
    },
    {
      label: 'Pilih Kendaraan',
      field: new FormControl('', [Validators.required]),
      errors: {
        required: 'Kendaraan harus dipilih',
      },
      inputType: InputType.radio,
      options: [
        {
          value: 'Mobil',
          title: 'Mobil',
        },
        {
          value: 'Motor',
          title: 'Motor',
        },
        {
          value: 'Sepeda',
          title: 'Sepeda',
        },
      ]
    },
    {
      label: 'Alamat',
      field: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(50)]),
      errors: {
        required: 'Alamat wajib diisi',
        minlength: 'Alamat minimal 1 karakter',
        maxlength: 'Alamat maksimal 50 karakter',
      },
      inputType: InputType.text
    },
  ]

  ngOnInit() {
  }

  changeInput() {
    this.results = [...this.results, { question: this.control.label, answer: this.control.field.value }]
    this.isLoading = true;
    setTimeout(() => {
      this.questionIndex++
      this.control = this.questions[this.questionIndex]
      this.isLoading = false
    }, 500);

  }

  getErrorMessage() {
    const errorFields = this.control.field.errors
    if (errorFields) {
      return this.control.errors[Object.keys(errorFields)[0]]
    }
  }

  onSubmit() {
    if (this.control.field.valid) {
      this.changeInput()
    }
  }

}
