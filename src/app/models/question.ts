import { FormControl } from "@angular/forms";
import { InputType } from "../enums/input-type";

export interface Question {
  label: string;
  field: FormControl;
  errors: Object;
  inputType: InputType;
  options?: Option[];
}